using System;

namespace SagePaie.Poc.Function.Registration
{
    public static class Config
    {
        public static string RedisConnectionString => Environment.GetEnvironmentVariable("RedisConnectionString");
        public static string TenantId => Environment.GetEnvironmentVariable("TenantId");
        public static string SubscriptionId => Environment.GetEnvironmentVariable("SubscriptionId");
        public static string ClientId => Environment.GetEnvironmentVariable("ClientId");
        public static string ClientKey => Environment.GetEnvironmentVariable("ClientKey");
        public static string ApimUrl => Environment.GetEnvironmentVariable("ApimUrl");
        public static string ResourceGroup => Environment.GetEnvironmentVariable("ResourceGroup");

        public static string EnvironmentPrefix => "soppocdev";
            
        public static string AppGateway => EnvironmentPrefix + "appGw";
        public static string DefaultBackendAddressPool => AppGateway + "Bepool";
        public static string ScaleSet => EnvironmentPrefix;
        public static object VirtualNetwork => EnvironmentPrefix + "vnet";
    }
}