using System;
using System.Collections.Generic;
using StackExchange.Redis;

namespace SagePaie.Poc.Function.Registration
{
    public static class PocHelper
    {
        private static readonly ConnectionMultiplexer _redis;

        static PocHelper()
        {
            _redis = ConnectionMultiplexer.Connect(Config.RedisConnectionString);
        }

        public static IDatabase GetDatabase()
        {
            return _redis.GetDatabase();
        }

        public static string PickOne(IList<string> healthySites)
        {
            Random r = new Random();
            int index = r.Next(0, healthySites.Count);

            return healthySites[index];
        }
    }
}