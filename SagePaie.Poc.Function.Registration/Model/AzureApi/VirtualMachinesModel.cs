﻿namespace SagePaie.Poc.Function.Registration.Model.AzureApi.VirtualMachine
{

    public class VirtualMachinesModel
    {
        public Value[] value { get; set; }
    }

    public class Value
    {
        public string instanceId { get; set; }
        public Sku sku { get; set; }
        public Properties properties { get; set; }
        public Resource[] resources { get; set; }
        public string type { get; set; }
        public string location { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Sku
    {
        public string name { get; set; }
        public string tier { get; set; }
    }

    public class Properties
    {
        public bool latestModelApplied { get; set; }
        public string vmId { get; set; }
        public Hardwareprofile hardwareProfile { get; set; }
        public Storageprofile storageProfile { get; set; }
        public Osprofile osProfile { get; set; }
        public Networkprofile networkProfile { get; set; }
        public string provisioningState { get; set; }
    }

    public class Hardwareprofile
    {
    }

    public class Storageprofile
    {
        public Imagereference imageReference { get; set; }
        public Osdisk osDisk { get; set; }
        public object[] dataDisks { get; set; }
    }

    public class Imagereference
    {
        public string publisher { get; set; }
        public string offer { get; set; }
        public string sku { get; set; }
        public string version { get; set; }
    }

    public class Osdisk
    {
        public string osType { get; set; }
        public string name { get; set; }
        public string createOption { get; set; }
        public string caching { get; set; }
        public Manageddisk managedDisk { get; set; }
        public int diskSizeGB { get; set; }
    }

    public class Manageddisk
    {
        public string storageAccountType { get; set; }
        public string id { get; set; }
    }

    public class Osprofile
    {
        public string computerName { get; set; }
        public string adminUsername { get; set; }
        public Windowsconfiguration windowsConfiguration { get; set; }
        public object[] secrets { get; set; }
    }

    public class Windowsconfiguration
    {
        public bool provisionVMAgent { get; set; }
        public bool enableAutomaticUpdates { get; set; }
    }

    public class Networkprofile
    {
        public Networkinterface[] networkInterfaces { get; set; }
    }

    public class Networkinterface
    {
        public string id { get; set; }
    }

    public class Resource
    {
        public Properties1 properties { get; set; }
        public string type { get; set; }
        public string location { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Properties1
    {
        public bool autoUpgradeMinorVersion { get; set; }
        public Settings settings { get; set; }
        public string provisioningState { get; set; }
        public string publisher { get; set; }
        public string type { get; set; }
        public string typeHandlerVersion { get; set; }
    }

    public class Settings
    {
        public string[] fileUris { get; set; }
    }

}
