﻿namespace SagePaie.Poc.Function.Registration.Model.AzureApi.BackendHealth
{
    public class BackendHealthModel
    {
        public Backendaddresspool[] backendAddressPools { get; set; }
    }

    public class Backendaddresspool
    {
        public Backendaddresspool1 backendAddressPool { get; set; }
        public Backendhttpsettingscollection[] backendHttpSettingsCollection { get; set; }
    }

    public class Backendaddresspool1
    {
        public string name { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public Properties properties { get; set; }
        public string type { get; set; }
    }

    public class Properties
    {
        public string provisioningState { get; set; }
        public Backendipconfiguration[] backendIPConfigurations { get; set; }
        public Backendaddress[] backendAddresses { get; set; }
        public Urlpathmap[] urlPathMaps { get; set; }
        public Pathrule[] pathRules { get; set; }
    }

    public class Backendipconfiguration
    {
        public string id { get; set; }
    }

    public class Backendaddress
    {
        public string ipAddress { get; set; }
    }

    public class Urlpathmap
    {
        public string id { get; set; }
    }

    public class Pathrule
    {
        public string id { get; set; }
    }

    public class Backendhttpsettingscollection
    {
        public Backendhttpsettings backendHttpSettings { get; set; }
        public Server[] servers { get; set; }
    }

    public class Backendhttpsettings
    {
        public string name { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public Properties1 properties { get; set; }
        public string type { get; set; }
    }

    public class Properties1
    {
        public string provisioningState { get; set; }
        public int port { get; set; }
        public string protocol { get; set; }
        public string cookieBasedAffinity { get; set; }
        public bool pickHostNameFromBackendAddress { get; set; }
        public string affinityCookieName { get; set; }
        public object path { get; set; }
        public int requestTimeout { get; set; }
        public Probe probe { get; set; }
        public Urlpathmap1[] urlPathMaps { get; set; }
        public Pathrule1[] pathRules { get; set; }
    }

    public class Probe
    {
        public string id { get; set; }
    }

    public class Urlpathmap1
    {
        public string id { get; set; }
    }

    public class Pathrule1
    {
        public string id { get; set; }
    }

    public class Server
    {
        public string address { get; set; }
        public Ipconfiguration ipConfiguration { get; set; }
        public string health { get; set; }
        public string healthProbeLog { get; set; }
    }

    public class Ipconfiguration
    {
        public string id { get; set; }
    }
}