﻿namespace SagePaie.Poc.Function.Registration.Model.AzureApi.AppGateway
{

    public class AppGatewayModel
    {
        public string name { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public string type { get; set; }
        public string location { get; set; }
        public Props properties { get; set; }
    }

    public class Props
    {
        public string provisioningState { get; set; }
        public string resourceGuid { get; set; }
        public Sku sku { get; set; }
        public string operationalState { get; set; }
        public Gatewayipconfiguration[] gatewayIPConfigurations { get; set; }
        public object[] sslCertificates { get; set; }
        public object[] authenticationCertificates { get; set; }
        public Frontendipconfiguration[] frontendIPConfigurations { get; set; }
        public Frontendport[] frontendPorts { get; set; }
        public Backendaddresspool[] backendAddressPools { get; set; }
        public Backendhttpsettingscollection[] backendHttpSettingsCollection { get; set; }
        public Httplistener2[] httpListeners { get; set; }
        public Urlpathmap2[] urlPathMaps { get; set; }
        public Requestroutingrule2[] requestRoutingRules { get; set; }
        public Probe1[] probes { get; set; }
        public object[] redirectConfigurations { get; set; }
    }

    public class Sku
    {
        public string name { get; set; }
        public string tier { get; set; }
        public int capacity { get; set; }
    }

    public class Gatewayipconfiguration
    {
        public string name { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public Properties1 properties { get; set; }
        public string type { get; set; }
    }

    public class Properties1
    {
        public string provisioningState { get; set; }
        public Subnet subnet { get; set; }
    }

    public class Subnet
    {
        public string id { get; set; }
    }

    public class Frontendipconfiguration
    {
        public string name { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public string type { get; set; }
        public Properties2 properties { get; set; }
    }

    public class Properties2
    {
        public string provisioningState { get; set; }
        public string privateIPAllocationMethod { get; set; }
        public Publicipaddress publicIPAddress { get; set; }
        public Httplistener[] httpListeners { get; set; }
    }

    public class Publicipaddress
    {
        public string id { get; set; }
    }

    public class Httplistener
    {
        public string id { get; set; }
    }

    public class Frontendport
    {
        public string name { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public Properties3 properties { get; set; }
        public string type { get; set; }
    }

    public class Properties3
    {
        public string provisioningState { get; set; }
        public int port { get; set; }
        public Httplistener1[] httpListeners { get; set; }
    }

    public class Httplistener1
    {
        public string id { get; set; }
    }

    public class Backendaddresspool
    {
        public string name { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public Properties4 properties { get; set; }
        public string type { get; set; }
    }

    public class Properties4
    {
        public string provisioningState { get; set; }
        public Backendipconfiguration[] backendIPConfigurations { get; set; }
        public Backendaddress[] backendAddresses { get; set; }
        public Urlpathmap[] urlPathMaps { get; set; }
        public Pathrule[] pathRules { get; set; }
    }

    public class Backendipconfiguration
    {
        public string id { get; set; }
    }

    public class Backendaddress
    {
        public string ipAddress { get; set; }
    }

    public class Urlpathmap
    {
        public string id { get; set; }
    }

    public class Pathrule
    {
        public string id { get; set; }
    }

    public class Backendhttpsettingscollection
    {
        public string name { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public Properties5 properties { get; set; }
        public string type { get; set; }
    }

    public class Properties5
    {
        public string provisioningState { get; set; }
        public int port { get; set; }
        public string protocol { get; set; }
        public string cookieBasedAffinity { get; set; }
        public bool pickHostNameFromBackendAddress { get; set; }
        public string affinityCookieName { get; set; }
        public string path { get; set; }
        public int requestTimeout { get; set; }
        public Probe probe { get; set; }
        public Urlpathmap1[] urlPathMaps { get; set; }
        public Pathrule1[] pathRules { get; set; }
    }

    public class Probe
    {
        public string id { get; set; }
    }

    public class Urlpathmap1
    {
        public string id { get; set; }
    }

    public class Pathrule1
    {
        public string id { get; set; }
    }

    public class Httplistener2
    {
        public string name { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public Properties6 properties { get; set; }
        public string type { get; set; }
    }

    public class Properties6
    {
        public string provisioningState { get; set; }
        public Frontendipconfiguration1 frontendIPConfiguration { get; set; }
        public Frontendport1 frontendPort { get; set; }
        public string protocol { get; set; }
        public bool requireServerNameIndication { get; set; }
        public Requestroutingrule[] requestRoutingRules { get; set; }
    }

    public class Frontendipconfiguration1
    {
        public string id { get; set; }
    }

    public class Frontendport1
    {
        public string id { get; set; }
    }

    public class Requestroutingrule
    {
        public string id { get; set; }
    }

    public class Urlpathmap2
    {
        public string name { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public Properties7 properties { get; set; }
        public string type { get; set; }
    }

    public class Properties7
    {
        public string provisioningState { get; set; }
        public Defaultbackendaddresspool defaultBackendAddressPool { get; set; }
        public Defaultbackendhttpsettings defaultBackendHttpSettings { get; set; }
        public Pathrule2[] pathRules { get; set; }
        public Requestroutingrule1[] requestRoutingRules { get; set; }
    }

    public class Defaultbackendaddresspool
    {
        public string id { get; set; }
    }

    public class Defaultbackendhttpsettings
    {
        public string id { get; set; }
    }

    public class Pathrule2
    {
        public string name { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public Properties8 properties { get; set; }
        public string type { get; set; }
    }

    public class Properties8
    {
        public string provisioningState { get; set; }
        public string[] paths { get; set; }
        public Backendaddresspool1 backendAddressPool { get; set; }
        public Backendhttpsettings backendHttpSettings { get; set; }
    }

    public class Backendaddresspool1
    {
        public string id { get; set; }
    }

    public class Backendhttpsettings
    {
        public string id { get; set; }
    }

    public class Requestroutingrule1
    {
        public string id { get; set; }
    }

    public class Requestroutingrule2
    {
        public string name { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public Properties9 properties { get; set; }
        public string type { get; set; }
    }

    public class Properties9
    {
        public string provisioningState { get; set; }
        public string ruleType { get; set; }
        public Httplistener3 httpListener { get; set; }
        public Urlpathmap3 urlPathMap { get; set; }
    }

    public class Httplistener3
    {
        public string id { get; set; }
    }

    public class Urlpathmap3
    {
        public string id { get; set; }
    }

    public class Probe1
    {
        public string name { get; set; }
        public string id { get; set; }
        public string etag { get; set; }
        public Properties10 properties { get; set; }
        public string type { get; set; }
    }

    public class Properties10
    {
        public string provisioningState { get; set; }
        public string protocol { get; set; }
        public string path { get; set; }
        public int interval { get; set; }
        public int timeout { get; set; }
        public int unhealthyThreshold { get; set; }
        public bool pickHostNameFromBackendHttpSettings { get; set; }
        public int minServers { get; set; }
        public Match match { get; set; }
        public Backendhttpsetting[] backendHttpSettings { get; set; }
    }

    public class Match
    {
        public string body { get; set; }
        public string[] statusCodes { get; set; }
    }

    public class Backendhttpsetting
    {
        public string id { get; set; }
    }

}