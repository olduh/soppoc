namespace SagePaie.Poc.Function.Registration.Model
{
    public class SiteAndIp
    {
        public string SiteId { get; set; }
        public string Ip { get; set; }
    }
}