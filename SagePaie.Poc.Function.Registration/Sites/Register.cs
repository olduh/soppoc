using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using Newtonsoft.Json;
using SagePaie.Poc.Function.Registration.Model.AzureApi.AppGateway;
using SagePaie.Poc.Function.Registration.Rules;

namespace SagePaie.Poc.Function.Registration.Sites
{
    public static class Register
    {
        [FunctionName("Sites-Register")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "sites")] HttpRequest req,
            ILogger log,
            ExecutionContext context)
        {
            var payload = JsonConvert.DeserializeObject<dynamic>(req.ReadAsStringAsync().Result);

            var siteId = (string)payload.siteId;
            var ip = (string)payload.ip;

            log.LogInformation($"Register site: {siteId} - {payload.ip}");

            var db = PocHelper.GetDatabase();
            while (! await db.LockTakeAsync("register", 1, TimeSpan.FromSeconds(30)))
            {
                log.LogInformation("awaiting lock...");
                await Task.Delay(500);
            }
            
            // with siteId and private IP
            // we can create the backend pool, if not present already
            // then the "health check" will create the rule for the backend pool

            var token = await AzureApiHelper.GetAccessToken(Config.TenantId, Config.ClientId, Config.ClientKey);
            var appGateway = await AzureApiHelper.GetAppGateway(token);
            var appGatewaySnapshot = JsonConvert.SerializeObject(appGateway);

            Update.CreateBackend(appGateway, siteId, ip, log);
            Update.CreatePathRule(appGateway, siteId, log);

            if (appGatewaySnapshot != JsonConvert.SerializeObject(appGateway))
            {
                log.LogInformation($"Need to update App Gateway config");

                // something has changed!
                var result = await AzureApiHelper.UpdateAppGateway(token, appGateway);

                //@todo: what if etag changed before updates take place?
            }


            // this hash set stores ip -> siteID map but isn't cleaned, ie.
            // ip -> siteID is accurate, but the VM can be deallocated or deleted
            db.HashSet("ips", RedisValue.Unbox((string)payload.ip), RedisValue.Unbox((string)payload.siteId));

            db.LockRelease("register", 1);

            return new OkResult();
        }
    }
}
