using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace SagePaie.Poc.Function.Registration.Sites
{
    public static class List
    {
        [FunctionName("Sites-List")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "sites")] HttpRequest req,
            ILogger log, 
            ExecutionContext context)
        {
            var sites = RedisHelper.GetDatabase2().HashGetAll("sites");

            log.LogInformation(sites.Length + " site(s) registered");

            return new OkObjectResult(sites);
        }
    }
}
