using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using SagePaie.Poc.Function.Registration.Rules;

namespace SagePaie.Poc.Function.Registration.Sites
{
    public static class Unregister
    {
        [FunctionName("Sites-Unregister")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "delete", Route = "sites/{siteId}")] HttpRequest req,
            ILogger log, 
            ExecutionContext context,
            string siteId)
        {
            log.LogInformation($"Unregister site: {siteId}");

            var db = PocHelper.GetDatabase();
            while (!await db.LockTakeAsync("register", 1, TimeSpan.FromSeconds(30)))
            {
                log.LogInformation("awaiting lock...");
                await Task.Delay(500);
            }

            var token = await AzureApiHelper.GetAccessToken(Config.TenantId, Config.ClientId, Config.ClientKey);
            var appGateway = await AzureApiHelper.GetAppGateway(token);

            if (appGateway.properties.backendAddressPools.Any(x => x.name == "site-" + siteId))
            {
                // yes!
                log.LogInformation($"There is a backend address pool for {siteId}");

                Update.UnlinkRuleFromBackend(appGateway, siteId, log);
                Update.RemovePathRule(appGateway, siteId, log);
                Update.UntrackTenants(db, siteId, log);
                Update.DeleteBackend(appGateway, siteId, log);

                var result = await AzureApiHelper.UpdateAppGateway(token, appGateway);
            }


            //db.HashDelete("sites", siteId);
            db.LockRelease("register", 1);

            return new OkResult();
        }
    }
}
