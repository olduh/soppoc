using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace SagePaie.Poc.Function.Registration.Routing
{
    public static class InitialRequest
    {
        [FunctionName("Routing-initialRequest")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "routing/initialRequest")]
            HttpRequest req,
            ILogger log,
            ExecutionContext context)
        {
            var value = await PocHelper.GetDatabase().StringGetAsync("healthySites");

            if (!value.HasValue)
            {
                log.LogCritical("No healthySites entry in Redis");
                return new OkObjectResult(
                    new
                    {
                        siteId = "-1",
                        baseUrl = "https://maintenance.sage.fr/"
                    });
            }

            var healthySites = ((string)value).Split(';', StringSplitOptions.RemoveEmptyEntries).Skip(1).ToList();

            if (healthySites.Count == 0)
            {
                log.LogCritical("Empty healthySites entry in Redis");
                return new OkObjectResult(
                    new
                    {
                        siteId = "-1",
                        baseUrl = "https://maintenance.sage.fr/"
                    });
            }

            var siteId = PocHelper.PickOne(healthySites);

            return new OkObjectResult(
                new
                {
                    siteId = siteId,
                    baseUrl = $"{Config.ApimUrl}/site/{siteId}/"
                });
        }
    }
}