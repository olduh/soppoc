using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace SagePaie.Poc.Function.Registration.Routing
{
    public static class RouteToTenant
    {
        [FunctionName("Routing-tenant")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "routing/tenant/{tenantId}")]
            HttpRequest req,
            ILogger log,
            ExecutionContext context,
            string tenantId)
        {
            string siteId;

            log.LogInformation("Request routing info for tenant " + tenantId);

            var db = PocHelper.GetDatabase();
            var trackingInfo = await db.HashGetAsync("trackings", tenantId);

            if (trackingInfo.HasValue)
            {
                // yes, tenant is already loaded
                siteId = (string)trackingInfo;
            }
            else
            {
                // register the tenant on any healthy host

                var value = await PocHelper.GetDatabase().StringGetAsync("healthySites");
                var healthySites = ((string) value).Split(';', StringSplitOptions.RemoveEmptyEntries).Skip(1).ToList();

                if (healthySites.Count == 0)
                {
                    return new UnprocessableEntityObjectResult(new
                    {
                        message = "No healthy site found"
                    });
                }

                siteId = PocHelper.PickOne(healthySites);

                await db.HashSetAsync("trackings", tenantId, siteId);
            }

            log.LogInformation(tenantId + " -> " + siteId);

            return new OkObjectResult(
                new
                {
                    siteId = siteId,
                    baseUrl = $"{Config.ApimUrl}/site/{siteId}/"
                });
        }
    }
}