using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace SagePaie.Poc.Function.Registration.Tracking
{
    public static class List
    {
        [FunctionName("Tracking-List")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "trackings")] HttpRequest req,
            ILogger log, 
            ExecutionContext context)
        {
            var trackings = PocHelper.GetDatabase().HashGetAll("trackings");

            log.LogInformation(trackings.Length + " tracking(s) registered");

            return new OkObjectResult(trackings);
        }
    }
}
