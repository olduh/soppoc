using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using Newtonsoft.Json;

namespace SagePaie.Poc.Function.Registration.Tracking
{
    public static class Register
    {
        [FunctionName("Tracking-Register")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "trackings")] HttpRequest req,
            ILogger log,
            ExecutionContext context)
        {
            var payload = JsonConvert.DeserializeObject<dynamic>(req.ReadAsStringAsync().Result);

            log.LogInformation($"Register tracking: {payload.tenantId} - {payload.siteId}");

            var db = RedisHelper.GetDatabase2();
            db.HashSet("trackings", RedisValue.Unbox((string)payload.tenantId), RedisValue.Unbox((string)payload.siteId));

            return new OkResult();
        }
    }
}
