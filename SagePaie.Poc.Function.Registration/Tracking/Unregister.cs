using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace SagePaie.Poc.Function.Registration.Tracking
{
    public static class Unregister
    {
        [FunctionName("Tracking-Unregister")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "delete", Route = "trackings/{tenantId}")] HttpRequest req,
            ILogger log, 
            ExecutionContext context,
            string tenantId)
        {
            log.LogInformation($"Unregister tracking for tenant: {tenantId}");

            var db = PocHelper.GetDatabase();

            if (tenantId == "all")
            {
                db.KeyDelete("trackings");
            }
            else
            {
                db.HashDelete("trackings", tenantId);
            }

            return new OkResult();
        }
    }
}
