using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json;
using SagePaie.Poc.Function.Registration.Model.AzureApi.AppGateway;
using SagePaie.Poc.Function.Registration.Model.AzureApi.BackendHealth;
using SagePaie.Poc.Function.Registration.Model.AzureApi.VirtualMachine;

namespace SagePaie.Poc.Function.Registration
{
    public static class AzureApiHelper
    {
        public static async Task<string> GetAccessToken(string tenantId, string clientId, string clientKey)
        {
            var authContextUrl = "https://login.windows.net/" + tenantId;
            var authenticationContext = new AuthenticationContext(authContextUrl);
            var credential = new ClientCredential(clientId, clientKey);
            var result = await authenticationContext.AcquireTokenAsync("https://management.azure.com/", credential);
            if (result == null)
            {
                throw new InvalidOperationException("Failed to obtain the JWT token");
            }
            string token = result.AccessToken;
            return token;
        }

        public static async Task<BackendHealthModel> GetBackendHealth(string token, ILogger log)
        {
            string url = $"https://management.azure.com/subscriptions/{Config.SubscriptionId}/resourceGroups/{Config.ResourceGroup}/providers/Microsoft.Network/applicationGateways/{Config.AppGateway}/backendhealth?api-version=2018-08-01&$expand=all";
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Remove("Authorization");
            httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpResponseMessage response = await httpClient.PostAsync(url, null);

            HttpResponseMessage response2;
            int retryCount = 0;
            do
            {
                if (retryCount > 50)
                {
                    return null;
                }

                response2 = await httpClient.GetAsync(response.Headers.Location);
                await Task.Delay(50);

                retryCount++;

            } while (response2.StatusCode == HttpStatusCode.Accepted);

            log.LogInformation("backend health received after " + retryCount + " attempts.");

            if (response2.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }

            var results2 = response2.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<BackendHealthModel>(results2);
        }

        public static async Task<AppGatewayModel> GetAppGateway(string token)
        {
            string url = $"https://management.azure.com/subscriptions/{Config.SubscriptionId}/resourceGroups/{Config.ResourceGroup}/providers/Microsoft.Network/applicationGateways/{Config.AppGateway}?api-version=2018-08-01";
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Remove("Authorization");
            httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpResponseMessage response = await httpClient.GetAsync(url);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }

            var results = response.Content.ReadAsStringAsync().Result;
            var settings = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            return JsonConvert.DeserializeObject<AppGatewayModel>(results, settings);
        }

        public static async Task<AppGatewayModel> UpdateAppGateway(string token, AppGatewayModel appGateway)
        {
            string url = $"https://management.azure.com/subscriptions/{Config.SubscriptionId}/resourceGroups/{Config.ResourceGroup}/providers/Microsoft.Network/applicationGateways/{Config.AppGateway}?api-version=2018-08-01";
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Remove("Authorization");
            httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            var json = JsonConvert.SerializeObject(appGateway, settings);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await httpClient.PutAsync(url, stringContent);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }

            var results = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<AppGatewayModel>(results);
        }

        public static async Task<VirtualMachinesModel> GetVirtualMachines(string token)
        {
            string url = $"https://management.azure.com/subscriptions/{Config.SubscriptionId}/resourceGroups/{Config.ResourceGroup}/providers/Microsoft.Compute/virtualMachineScaleSets/{Config.ScaleSet}/virtualMachines?api-version=2018-06-01";
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Remove("Authorization");
            httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpResponseMessage response = await httpClient.GetAsync(url);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }

            var results = response.Content.ReadAsStringAsync().Result;
            var settings = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            return JsonConvert.DeserializeObject<VirtualMachinesModel>(results, settings);
        }

        public static async Task<dynamic> GetVirtualNetwork(string token)
        {
            string url = $"https://management.azure.com/subscriptions/{Config.SubscriptionId}/resourceGroups/{Config.ResourceGroup}/providers/Microsoft.Network/virtualNetworks/{Config.VirtualNetwork}?api-version=2018-08-01";
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Remove("Authorization");
            httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            HttpResponseMessage response = await httpClient.GetAsync(url);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }

            var results = response.Content.ReadAsStringAsync().Result;
            var settings = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            return JsonConvert.DeserializeObject<dynamic>(results, settings);
        }
    }
}