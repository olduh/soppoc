using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace SagePaie.Poc.Function.Registration.Dashboard
{
    public static class BackendHealth
    {
        [FunctionName("Dashboard-BackendHealth")]
        public static async Task<IActionResult> Get(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "dashboard/backendHealth")]
            HttpRequest req,
            ILogger log,
            ExecutionContext context)
        {
            var token = await AzureApiHelper.GetAccessToken(Config.TenantId, Config.ClientId, Config.ClientKey);

            var backendHealth = await AzureApiHelper.GetBackendHealth(token, log);

            return new OkObjectResult(backendHealth);
        }
    }
}