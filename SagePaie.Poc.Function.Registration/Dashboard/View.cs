using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace SagePaie.Poc.Function.Registration.Dashboard
{
    public static class View
    {
        [FunctionName("Dashboard-View")]
        public static async Task<IActionResult> Get(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "dashboard/view")]
            HttpRequest req,
            ILogger log,
            ExecutionContext context)
        {
            var report = string.Empty;

            //
            // healthySites
            //

            var value = await PocHelper.GetDatabase().StringGetAsync("healthySites");
            
            if (!value.HasValue)
            {
                report += "Healthy Sites - not found in Redis" + Environment.NewLine;
            }
            else
            {
                var healthySites = ((string)value).Split(';', StringSplitOptions.RemoveEmptyEntries).ToList();
                report += $"Healthy Sites - last update @{healthySites[0]} - {(healthySites.Count - 1)} site(s){Environment.NewLine}";
                foreach (var info in healthySites.Skip(1))
                {
                    report += "- " + info + Environment.NewLine;
                }
            }

            report += Environment.NewLine;
            report += Environment.NewLine;

            //
            // ips
            //

            report += "IPs" + Environment.NewLine;

            foreach (var entry in (await PocHelper.GetDatabase().HashGetAllAsync("ips")).OrderBy(x => x.Name))
            {
                report += "- " + entry.Name + " -> " + entry.Value + Environment.NewLine;
            }

            return new OkObjectResult(report);
        }
    }
}