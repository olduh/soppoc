using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SagePaie.Poc.Function.Registration.Model;
using SagePaie.Poc.Function.Registration.Model.AzureApi.AppGateway;
using SagePaie.Poc.Function.Registration.Model.AzureApi.BackendHealth;
using StackExchange.Redis;
using Backendaddress = SagePaie.Poc.Function.Registration.Model.AzureApi.AppGateway.Backendaddress;
using Backendaddresspool = SagePaie.Poc.Function.Registration.Model.AzureApi.AppGateway.Backendaddresspool;
using Pathrule = SagePaie.Poc.Function.Registration.Model.AzureApi.AppGateway.Pathrule;

namespace SagePaie.Poc.Function.Registration.Rules
{
    public static class Update
    {
        [FunctionName("Rules-Update")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "rules")] HttpRequest req,
            ILogger log,
            ExecutionContext context)
        {
            await UpdateRules(log);

            return new OkResult();
        }

        [FunctionName("Rules-Update-Trigger")]
        public static async void Run2([TimerTrigger("00:00:20")]TimerInfo myTimer, ILogger log)
        {
            await UpdateRules(log);
        }

        private static async Task UpdateRules(ILogger log)
        {
            var token = await AzureApiHelper.GetAccessToken(Config.TenantId, Config.ClientId, Config.ClientKey);

            //var ip = await AzureApiHelper.GetRandom(token, "https://management.azure.com/subscriptions/446a7f77-28f6-4987-89e2-b4035019f52b/resourceGroups/soppoc20/providers/Microsoft.Compute/virtualMachineScaleSets/soppocdev/virtualMachines/0/networkInterfaces/soppocdevnic/ipConfigurations/soppocdevipconfig");

            //var vms = await AzureApiHelper.GetVirtualMachines(token);
            //var names = vms.value.Select(x => x.name).ToList();

            //var vn = await AzureApiHelper.GetVirtualNetwork(token);
           

            //return;

            var backendhealth = await AzureApiHelper.GetBackendHealth(token, log);

            if (backendhealth != null)
            {
                var appGateway = await AzureApiHelper.GetAppGateway(token);
                var appGatewaySnapshot = JsonConvert.SerializeObject(appGateway);

                var db = PocHelper.GetDatabase();

                // Redis contains a list of ips -> sites
                // Some of the ips may no longer exists
                var sitesAndIps = new List<SiteAndIp>();
                foreach (var entry in db.HashGetAll("ips"))
                {
                    sitesAndIps.Add(new SiteAndIp
                    {
                        Ip = entry.Name,
                        SiteId = entry.Value
                    });
                }

                var servers = backendhealth.backendAddressPools.SelectMany(x => x.backendHttpSettingsCollection.SelectMany(y => y.servers))
                                                               .ToList();
                var uniqueServers = new List<Server>();
                foreach (var server in servers)
                {
                    if (uniqueServers.All(x => x.address != server.address))
                    {
                        uniqueServers.Add(server);
                    }
                }

                var unhealthySites = new List<string>();
                var healthySites = new List<string>();

                foreach (var siteAndIp in sitesAndIps)
                {
                    var healthInfo = uniqueServers.FirstOrDefault(x => x.address == siteAndIp.Ip);

                    if (healthInfo == null)
                    {
                        log.LogInformation($"{siteAndIp.Ip} not found in backend health, we assume it is unhealthy");
                        unhealthySites.Add(siteAndIp.SiteId);

                        continue;
                    }

                    if (healthInfo.health != "Healthy")
                    {
                        log.LogInformation($"{healthInfo.address} unhealthy");
                        unhealthySites.Add(siteAndIp.SiteId);
                    }
                    else
                    {
                        log.LogInformation($"{healthInfo.address} healthy");
                        healthySites.Add(siteAndIp.SiteId);
                    }
                }

                
                //
                // store in Redis the healthy sites
                // this will be used by APIM to route initial requests
                //
                var timestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                var healthySitesId = string.Join(';', healthySites.OrderBy(x => x));
                db.StringSet("healthySites", $"{timestamp};{healthySitesId}");

                
                //
                // unhealthy sites: remove pathRule, if not yet done,
                // - from urlPathMaps (sopRule)
                // - from the backend pool of the site
                // 
                foreach (var unhealthySite in unhealthySites)
                {
                    RemovePathRule(appGateway, unhealthySite, log);
                    UnlinkRuleFromBackend(appGateway, unhealthySite, log);
                    DeleteBackend(appGateway, unhealthySite, log);
                    UntrackTenants(db, unhealthySite, log);
                }

                //
                // healthy sites: add pathRule, if not yet done,
                // - to urlPathMaps (sopRule)
                // - to the backend pool
                //

                foreach (var healthySite in healthySites)
                {
                    var ip = sitesAndIps.First(x => x.SiteId == healthySite).Ip;

                    CreateBackend(appGateway, healthySite, ip, log);
                    CreatePathRule(appGateway, healthySite, log);
                    LinkRuleToBackend(appGateway, healthySite, log);
                }

                //
                // it should not happen, but we cannot exclude it (because of unexpected shutdowns)
                // remove backendpools with no rules
                //
                var countBefore = appGateway.properties.backendAddressPools.Length;
                appGateway.properties.backendAddressPools = appGateway.properties.backendAddressPools.Where(x => x.properties.pathRules?.Length > 0).ToArray();
                var countAfter = appGateway.properties.backendAddressPools.Length;
                if (countAfter != countBefore)
                {
                    log.LogInformation($"Found {countBefore - countAfter} dead pools (= with 0 rule) -> they will be deleted.");
                }

                // check for dead pools (ie. pools with no rule

                if (appGatewaySnapshot != JsonConvert.SerializeObject(appGateway))
                {
                    log.LogInformation($"Need to update App Gateway config"); 

                    // something has changed!
                    var result = await AzureApiHelper.UpdateAppGateway(token, appGateway);

                    //@todo: what if etag changed before updates take place?
                }
                
            }
        }

        public static void UntrackTenants(IDatabase db, string siteId, ILogger log)
        {
            var trackings = db.HashGetAll("trackings");
            foreach (var hashEntry in trackings)
            {
                if (hashEntry.Value == siteId)
                {
                    log.LogInformation($"Untracking {hashEntry.Name} from {siteId}");
                    db.HashDelete("trackings", hashEntry.Name);
                }
            }
        }

        public static void UnlinkRuleFromBackend(AppGatewayModel appGateway, string siteId, ILogger log)
        {
            for (int i = 0; i < appGateway.properties.backendAddressPools.Length; i++)
            {
                var backendAddressPool = appGateway.properties.backendAddressPools[i];

                if (backendAddressPool.properties.pathRules != null &&
                    backendAddressPool.properties.pathRules.Any(x => x.id.EndsWith("/" + siteId)))
                {
                    log.LogInformation($"Unlinking rule from backend {backendAddressPool.name} for {siteId}");

                    backendAddressPool.properties.pathRules = backendAddressPool.properties.pathRules?.Where(x => !x.id.EndsWith("/" + siteId)).ToArray();
                }
            }
        }

        public static void RemovePathRule(AppGatewayModel appGateway, string siteId, ILogger log)
        {
            //@todo: we assume there is only one urlPathMaps
            if (appGateway.properties.urlPathMaps[0].properties.pathRules.Any(x => x.name == siteId))
            {
                log.LogInformation($"Removing path rule for {siteId}");

                appGateway.properties.urlPathMaps[0].properties.pathRules = appGateway.properties.urlPathMaps[0].properties.pathRules.Where(x => x.name != siteId).ToArray();
            }
        }

        public static void LinkRuleToBackend(AppGatewayModel appGateway, string siteId, ILogger log)
        {
            for (int i = 0; i < appGateway.properties.backendAddressPools.Length; i++)
            {
                var backendAddressPool = appGateway.properties.backendAddressPools[i];

                if (backendAddressPool.name == "site-" + siteId)
                {
                    if (backendAddressPool.properties.pathRules == null || 
                        !backendAddressPool.properties.pathRules.Any(x => x.id.EndsWith(siteId)))
                    {
                        log.LogInformation($"Linking rule to backend for {siteId}");

                        // there is no rule for the backend pool
                        var newRule = new Pathrule()
                        {
                            id = $"/subscriptions/{Config.SubscriptionId}/resourceGroups/{Config.ResourceGroup}/providers/Microsoft.Network/applicationGateways/{Config.AppGateway}/urlPathMaps/sopRule/pathRules/" + siteId
                        };

                        if (backendAddressPool.properties.pathRules == null)
                        {
                            backendAddressPool.properties.pathRules = new Model.AzureApi.AppGateway.Pathrule[0];
                        }

                        backendAddressPool.properties.pathRules = backendAddressPool.properties.pathRules.Append(newRule).ToArray();
                    }
                }
            }
        }

        public static void CreatePathRule(AppGatewayModel appGateway, string siteId, ILogger log)
        {
            //@todo: we assume there is only one urlPathMaps
            if (!appGateway.properties.urlPathMaps[0].properties.pathRules.Any(x => x.name == siteId))
            {
                log.LogInformation($"Creating path rule for {siteId}");

                // there is no rule for this site
                var newRule = new Pathrule2()
                {
                    name = siteId,
                    id = appGateway.properties.urlPathMaps[0].id + "/pathRules/" + siteId,
                    properties = new Properties8()
                    {
                        backendAddressPool = new Model.AzureApi.AppGateway.Backendaddresspool1()
                        {
                            id = $"/subscriptions/{Config.SubscriptionId}/resourceGroups/{Config.ResourceGroup}/providers/Microsoft.Network/applicationGateways/{Config.AppGateway}/backendAddressPools/site-" + siteId
                        },
                        backendHttpSettings = new Model.AzureApi.AppGateway.Backendhttpsettings()
                        {
                            id = $"/subscriptions/{Config.SubscriptionId}/resourceGroups/{Config.ResourceGroup}/providers/Microsoft.Network/applicationGateways/{Config.AppGateway}/backendHttpSettingsCollection/appGwBackendHttpSettings"
                        },
                        paths = new[] { "/site/" + siteId + "/*" }
                    }
                };

                appGateway.properties.urlPathMaps[0].properties.pathRules = appGateway.properties.urlPathMaps[0].properties.pathRules.Append(newRule).OrderBy(x => x.name).ToArray();
            }
        }

        public static void DeleteBackend(AppGatewayModel appGateway, string siteId, ILogger log)
        {
            if (appGateway.properties.backendAddressPools.Any(x => x.name == "site-" + siteId))
            {
                log.LogInformation($"Deleting backend address pool for {siteId}");

                appGateway.properties.backendAddressPools = appGateway.properties.backendAddressPools.Where(x => x.name != "site-" + siteId).ToArray();
            }
        }

        public static void CreateBackend(AppGatewayModel appGateway, string siteId, string ip, ILogger log)
        {
            // is there a backend pool for this site ID ?
            if (appGateway.properties.backendAddressPools.Any(x => x.name == "site-" + siteId))
            {
                // yes!
                log.LogInformation($"There is already a backend address pool for {siteId}");
            }
            else
            {
                var defaultPool = appGateway.properties.backendAddressPools[0];

                // no
                var newBackend = new Backendaddresspool
                {
                    name = "site-" + siteId,
                    properties = new Properties4
                    {
                        backendIPConfigurations = defaultPool.properties.backendIPConfigurations,
                        urlPathMaps = defaultPool.properties.urlPathMaps,
                        backendAddresses = new[]
                        {
                            new Backendaddress
                            {
                                ipAddress = ip
                            }
                        }
                    }
                };

                appGateway.properties.backendAddressPools = appGateway.properties.backendAddressPools.Append(newBackend).ToArray();

                log.LogInformation($"Creating backend address pool for {siteId}");
            }
        }
    }
}
