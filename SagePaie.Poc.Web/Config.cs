using System;
using System.Configuration;
using StackExchange.Redis;

namespace SagePaie.Poc.Web
{
    public static class Config
    {
        public static string DefaultPassword => ConfigurationManager.AppSettings["DefaultPassword"];
        public static string CurrentSiteId => ConfigurationManager.AppSettings["CurrentSiteId"];
        public static string Fqdn => ConfigurationManager.AppSettings["Fqdn"];
        public static string Environment => ConfigurationManager.AppSettings["Environment"];
        public static string SendGridLogin => ConfigurationManager.AppSettings["SendGridLogin"];
        public static string SendGridPassword => ConfigurationManager.AppSettings["SendGridPassword"];
        public static string RedisConnectionString => ConfigurationManager.AppSettings["RedisConnectionString"];
        public static string AzureFunctionRootUrl => ConfigurationManager.AppSettings["AzureFunctionRootUrl"];
        public static string AzureFunctionCode => ConfigurationManager.AppSettings["AzureFunctionCode"];

        public static bool UseRedisForTenantAffinityCheck { get; set; }
        public static string PrivateIp { get; set; }
    }
}