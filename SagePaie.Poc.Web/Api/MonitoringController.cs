﻿using System.Web.Http;

namespace SagePaie.Poc.Web.Api
{
    public class MonitoringController : ApiController
    {
        [HttpGet]
        [Route("monitoring/status")]
        public IHttpActionResult Get()
        {
            return Ok();
        }
    }
}