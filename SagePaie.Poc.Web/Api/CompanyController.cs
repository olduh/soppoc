﻿using System;
using System.Linq;
using System.Web.Http;
using SagePaie.Poc.Web.Front;

namespace SagePaie.Poc.Web.Api
{
    public class CompanyController : ApiController
    {
        [HttpGet]
        [Route("api/company")]
        public IHttpActionResult Get()
        {
            var tenantId = Request.Headers.GetValues("X-TenantId").FirstOrDefault();
            
            return Ok(new
            {
                tenantId = tenantId,
                tick = DateTime.UtcNow.Ticks,
                currentSiteId = Config.CurrentSiteId,
                privateIp = Config.PrivateIp
            });
        }
    }
}