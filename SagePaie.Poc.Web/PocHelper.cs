using System;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Web;
using log4net;
using Newtonsoft.Json;
using SagePaie.Poc.Web.Orchestra;
using StackExchange.Redis;

namespace SagePaie.Poc.Web
{
    public static class PocHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(PocHelper));
        private static ConnectionMultiplexer _redis;

        static PocHelper()
        {
            _redis = ConnectionMultiplexer.Connect(Config.RedisConnectionString);
        }

        public static IDatabase GetRedisDatabase()
        {
            return _redis.GetDatabase();
        }

        public static string AzureFunctionGet(string route)
        {
            var azureFunctionUrl = Config.AzureFunctionRootUrl;
            var azureFunctionCode = Config.AzureFunctionCode;

            using (var client = new HttpClient())
            {
                var url = $"{azureFunctionUrl}/{route}?code={azureFunctionCode}";

                HttpResponseMessage response = client.GetAsync(url).Result;

                Log.Info("AzureFunctionGet = " + response.StatusCode);

                var results = response.Content.ReadAsStringAsync().Result;

                return results;
            }
        }

        public static string AzureFunctionPost(string route, object o)
        {
            var azureFunctionUrl = Config.AzureFunctionRootUrl;
            var azureFunctionCode = Config.AzureFunctionCode;

            var client = new HttpClient();
            var url = $"{azureFunctionUrl}/{route}?code={azureFunctionCode}";
            
            HttpResponseMessage response = client.PostAsJsonAsync(url, o).Result;
            var results = response.Content.ReadAsStringAsync().Result;

            return results;
        }

        public static string AzureFunctionDelete(string route)
        {
            var azureFunctionUrl = Config.AzureFunctionRootUrl;
            var azureFunctionCode = Config.AzureFunctionCode;

            var client = new HttpClient();
            var url = $"{azureFunctionUrl}/{route}?code={azureFunctionCode}";
            
            HttpResponseMessage response = client.DeleteAsync(url).Result;
            var results = response.Content.ReadAsStringAsync().Result;

            return results;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static void SendMail(string subject)
        {
            MailMessage mail = new MailMessage("soppoc@sop.sage.com", "francois.baronnet@sage.com");
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Port = 587;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Host = "smtp.sendgrid.net";
            smtpClient.Credentials = new NetworkCredential(Config.SendGridLogin, Config.SendGridPassword);
            mail.Subject = subject;
            mail.Body = "[nothing to say]";
            smtpClient.Send(mail);
        }

        public static void LogInfo(string message)
        {
            Log.Info(message);
        }

        public static string CurrentCompanyName()
        {
            var ctxKey = HttpContext.Current.Request["ctxKey"];
            var sopSession = JsonConvert.DeserializeObject<SopSession>(PocHelper.Base64Decode(ctxKey));
            return sopSession.TenantId;
        }
    }
}