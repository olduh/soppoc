﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Companies.aspx.cs" Inherits="SagePaie.Poc.Web.Orchestra.Companies" %>

<%@ Import Namespace="SagePaie.Poc.Web" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Companies</h2>
    <h3>Orchestra - List of all companies</h3>
    <p>This section is for Sage users only.</p>

    <table>
        <tr>
            <th width="200px">Tenant ID</th>
            <th width="200px">Company Name</th>
            <th width="200px">Site ID</th>
            <th width="200px">Access this company</th>
        </tr>
        <%
            var db = PocHelper.GetRedisDatabase();
            var trackings = db.HashGetAll("trackings");


            foreach (var companyEntry in db.HashGetAll("companies"))
            {
        %>
        <tr>
            <td><%= companyEntry.Name %></td>
            <td><%= companyEntry.Value %></td>
            <td><%
                    var tracking = trackings.FirstOrDefault(x => x.Name == companyEntry.Name);
                    if (tracking != null)
                    {
                        %>
                        <%= (string)tracking.Value %>
                        <%
                    }
                %></td>
            <td><a href="Companies.aspx?action=go&tenantId=<%= companyEntry.Name %>">go</a></td>
        </tr>
        <%
            }

        %>
    </table>

</asp:Content>
