﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Sites.aspx.cs" Inherits="SagePaie.Poc.Web.Orchestra.Sites" %>

<%@ Import Namespace="SagePaie.Poc.Web" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Sites</h2>
    <h3>Orchestra - List of all sites</h3>
    <p>This section is for Sage users only.</p>

    <table>
        <tr>
            <th width="200px">Site ID</th>
            <th width="200px">Private IP</th>
        </tr>
        <%

            foreach (var siteEntry in PocHelper.GetRedisDatabase().HashGetAll("sites"))
            {
        %>
        <tr>
            <td><%= siteEntry.Name %></td>
            <td><%= siteEntry.Value %></td>
        </tr>
        <%
            }

        %>
    </table>

</asp:Content>
