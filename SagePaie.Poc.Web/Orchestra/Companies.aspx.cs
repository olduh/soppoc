﻿using System;
using System.Web;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace SagePaie.Poc.Web.Orchestra
{
    public partial class Companies : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request["action"] == "go")
            {
                // no loading logic here
                // we just build a link with the tenantId wrapped in context
                // the app will redirect later the user to the right site
                // later = the next request :)

                var tenantId = this.Request["tenantId"];
                
                var sopSession = new SopSession
                {
                    TenantId = tenantId
                };

                var ctxKey = PocHelper.Base64Encode(JsonConvert.SerializeObject(sopSession));
                Response.Redirect("~/Front/Home.aspx?ctxKey=" + ctxKey);
            }
        }
    }
}