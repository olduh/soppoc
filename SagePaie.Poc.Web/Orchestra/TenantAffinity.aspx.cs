﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SagePaie.Poc.Web.Orchestra
{
    public partial class TenantAffinity : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                chkRedis.Checked = Config.UseRedisForTenantAffinityCheck;
            }
        }

        protected void butReset_OnClick(object sender, EventArgs e)
        {
            var result = PocHelper.AzureFunctionDelete($"api/trackings/all");
            Response.Redirect(Request.RawUrl);
        }

        protected void chkRedis_OnCheckedChanged(object sender, EventArgs e)
        {
            Config.UseRedisForTenantAffinityCheck = chkRedis.Checked;
        }
    }
}