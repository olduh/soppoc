﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="TenantAffinity.aspx.cs" Inherits="SagePaie.Poc.Web.Orchestra.TenantAffinity" %>

<%@ Import Namespace="SagePaie.Poc.Web" %>
<%@ Import Namespace="Newtonsoft.Json" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Tenant Affinity</h2>
    <h3>Orchestra - On which site is what tenant?</h3>
    <p>This section is for Sage users only.</p>
    
    <p>
        <asp:Button runat="server" Text="Reset tracking" ID="butReset" OnClick="butReset_OnClick"/>
        <br/>
        <asp:CheckBox runat="server" 
                      Text="Talk directly to Redis to check tenant affinity?" 
                      ID="chkRedis" 
                      OnCheckedChanged="chkRedis_OnCheckedChanged" 
                      AutoPostBack="True"/>
    </p>

    <table>
        <tr>
            <th width="200px">Tenant ID</th>
            <th width="200px">Site ID</th>
        </tr>
        <%
            var info = PocHelper.AzureFunctionGet("api/trackings");

            if (!string.IsNullOrEmpty(info))
            {
                var trackings = JsonConvert.DeserializeObject<dynamic>(info);

                foreach (var trackingEntry in trackings)
                {
            %>
                    <tr>
                        <td><%= trackingEntry.name %></td>
                        <td><%= trackingEntry.value %></td>
                    </tr>
            <%
                }
            }
        %>
    </table>

</asp:Content>
