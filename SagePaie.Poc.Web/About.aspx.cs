﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SagePaie.Poc.Web
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnSubmit.Click += (x, y) => litSomeText.Text = tbxSomeText.Text;
        }
    }
}