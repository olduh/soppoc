﻿<%@ Page Language="C#" CodeBehind="Logon.aspx.cs" Inherits="SagePaie.Poc.Web.Public.Logon"  %>
<%@ Import Namespace="System.Web.Security" %>

<html>
<head id="Head1" runat="server">
    <title>Sage Business Cloud Paie - Login</title>
</head>
<body>
<form id="form1" runat="server">
    <center>
        <h3>Sage Business Cloud Paie</h3>
        <table>
            <tr>
                <td>
                    E-mail address:</td>
                <td>
                    <asp:TextBox ID="UserEmail" runat="server" /></td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                                ControlToValidate="UserEmail"
                                                Display="Dynamic" 
                                                ErrorMessage="Cannot be empty." 
                                                runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Password:</td>
                <td>
                    <asp:TextBox ID="UserPass" TextMode="Password" runat="server" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                                                ControlToValidate="UserPass"
                                                ErrorMessage="Cannot be empty." 
                                                runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Remember me?</td>
                <td>
                    <asp:CheckBox ID="Persist" runat="server" /></td>
            </tr>
        </table>
        <asp:Button ID="Submit1" OnClick="Logon_Click" Text="Log On" 
                    runat="server" />
        <p>
            <asp:Label ID="Msg" ForeColor="red" runat="server" />
        </p>
    </center>
</form>
</body>
</html>