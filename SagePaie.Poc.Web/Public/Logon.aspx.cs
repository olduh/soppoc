﻿using System;
using System.Web.Security;
using Newtonsoft.Json;
using SagePaie.Poc.Web.Orchestra;

namespace SagePaie.Poc.Web.Public
{
    public partial class Logon : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Logon_Click(object sender, EventArgs e)
        {
            if (UserEmail.Text != null && UserEmail.Text.EndsWith("@sop.sage.com") && UserPass.Text == Config.DefaultPassword)
            {
                FormsAuthentication.SetAuthCookie(UserEmail.Text, Persist.Checked);
                
                if (UserEmail.Text.StartsWith("BD"))
                {
                    // this is how we simulate a "payroll user"
                    // if email is like BDxxx@sop.sage.com
                    // then we redirect the user to tenant BDxxx

                    var tenantId = UserEmail.Text.Split('@')[0];

                    var sopSession = new SopSession
                    {
                        TenantId = tenantId
                    };

                    var ctxKey = PocHelper.Base64Encode(JsonConvert.SerializeObject(sopSession));
                    Response.Redirect("~/Front/Home.aspx?ctxKey=" + ctxKey);
                }

                Response.Redirect("~/");
            }
            else
            {
                Msg.Text = "Invalid credentials. Please try again.";
            }
        }
    }
}