﻿using System;
using System.Web;
using Newtonsoft.Json;
using SagePaie.Poc.Web.Orchestra;

namespace SagePaie.Poc.Web.Front
{
    public class StickySessionHelper
    {
        public static string GetTenantUriIfNotHere(HttpRequest request)
        {
            var t0 = DateTime.UtcNow;
            var result = GetTenantUriIfNotHereInternal(request);
            var duration = (DateTime.UtcNow - t0).TotalMilliseconds;

            PocHelper.LogInfo("stick session and tenant affinity check - " + (long)duration + " ms");

            return result;
        }

        private static string GetTenantUriIfNotHereInternal(HttpRequest request)
        {
            if (request["ctxKey"] == null)
            {
                return null;
            }

            var ctxKey = request["ctxKey"];
            var sopSession = JsonConvert.DeserializeObject<SopSession>(PocHelper.Base64Decode(ctxKey));
            var tenantId = sopSession.TenantId;

            //
            // quick lookup in Redis (depending on config)
            //

            if (Config.UseRedisForTenantAffinityCheck)
            {
                var trackingInfo = PocHelper.GetRedisDatabase().HashGet("trackings", tenantId);
                if (trackingInfo.HasValue)
                {
                    var targetSiteId = (string)trackingInfo;
                    if (targetSiteId == Config.CurrentSiteId)
                    {
                        return null;
                    }
                }
            }

            //
            // calls AzureFunction to know where the tenant is loaded
            // and if somewhere else, get the redirect base url
            //

            var result = PocHelper.AzureFunctionGet("api/routing/tenant/" + tenantId);

            if (result != null)
            {
                var routingInfo = JsonConvert.DeserializeObject<dynamic>(result);
                if (routingInfo.siteId != null)
                {
                    var siteId = (string)routingInfo.siteId;

                    if (siteId == Config.CurrentSiteId)
                    {
                        return null;
                    }

                    return routingInfo.baseUrl.ToString();
                }
            }

            return null;
        }
    }
}