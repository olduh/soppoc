﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Employees.aspx.cs" Inherits="SagePaie.Poc.Web.Front.Employees" %>

<%@ Import Namespace="SagePaie.Poc.Web" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <div>
        <%= PocHelper.CurrentCompanyName() %> | 
        <a href="Home.aspx?ctxKey=<%= Request["ctxKey"] %>">Home</a> | 
        <a href="Employees.aspx?ctxKey=<%= Request["ctxKey"] %>">Employees</a> | 
        <a href="Payruns.aspx?ctxKey=<%= Request["ctxKey"] %>">Payruns</a> 
    </div>
    
    <hr/>

    <h2>Employees</h2>
    <h3>Employees page of a Payroll Company</h3>
    <p>We are in the context of a tenant.</p>

</asp:Content>
