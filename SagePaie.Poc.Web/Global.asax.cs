﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Routing;
using log4net.Config;
using Newtonsoft.Json;
using SagePaie.Poc.Web.Front;

namespace SagePaie.Poc.Web
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            XmlConfigurator.Configure();

            log4net.GlobalContext.Properties["Source"] = Config.CurrentSiteId;

            if (Config.Environment != "Local")
            {
                //
                // Get PrivateIP from metadata service
                //
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Metadata", "true");
                var url = "http://169.254.169.254/metadata/instance?api-version=2017-08-01";
                HttpResponseMessage response = client.GetAsync(url).Result;
                var result = response.Content.ReadAsStringAsync().Result;

                var metadata = JsonConvert.DeserializeObject<dynamic>(result);

                var privateIp = metadata.network["interface"][0]["ipv4"]["ipAddress"][0]["privateIpAddress"].Value;

                Config.PrivateIp = privateIp;
            }
            else
            {
                Config.PrivateIp = "127.0.0.1";
            }

            //
            // Register current site
            //
            var obj = new
            {
                siteId = Config.CurrentSiteId,
                ip = Config.PrivateIp
            };
            var foo = PocHelper.AzureFunctionPost("api/sites", obj);

            PocHelper.LogInfo("Application_Start");
            PocHelper.SendMail("Application_Start - " + Config.CurrentSiteId + " - " + Config.PrivateIp);
        }

        void Application_End(object sender, EventArgs e)
        {
            //
            // Unregister current site
            //
            var foo = PocHelper.AzureFunctionDelete($"api/sites/{Config.CurrentSiteId}");

            PocHelper.LogInfo("Application_End");
            PocHelper.SendMail("Application_End - " + Config.CurrentSiteId + " - " + Config.PrivateIp);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (Request.Url.LocalPath.Contains("/Front/"))
            {
                // this requests targets the front-office, aka the payroll side of the website
                var redirectUri = StickySessionHelper.GetTenantUriIfNotHere(Request);
                if (redirectUri != null)
                {
                    var pathAndQuery = Request.Url.PathAndQuery;

                    if (pathAndQuery.StartsWith("/site/"))
                    {
                        pathAndQuery = "/" + string.Join("/", pathAndQuery.Split('/').Skip(3).ToArray());
                    }

                    Response.Redirect(redirectUri + pathAndQuery);
                }
            }
        }
    }
}