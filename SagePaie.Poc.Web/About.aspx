﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="SagePaie.Poc.Web.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>A page with a form</h3>
    <p>A very simple form to trigger POST requests.</p>
    Some text: <asp:TextBox runat="server" ID="tbxSomeText"/> <asp:Button runat="server" ID="btnSubmit" Text="Submit"/>
    <asp:Literal runat="server" ID="litSomeText" />
</asp:Content>
